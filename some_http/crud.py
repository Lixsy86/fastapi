from typing import List, Optional

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from some_http import models, schemas, security
from some_http.database import SessionLocal


def create_game(db: Session, game: schemas.PostGame) -> models.GameModel:
    db_game = models.GameModel(name=game.name, price=game.price)
    db.add(db_game)
    db.commit()
    return db_game


def create_user(db: Session, users: schemas.Users) -> models.UserModel:
    hash_pass = security.calculate_hash(users.password)
    db_user = models.UserModel(username=users.username, password=hash_pass)
    db.add(db_user)
    db.commit()

    return db_user


def create_first_user():
    db = SessionLocal()
    first_user: schemas.Users = schemas.Users(username="test", password="password")
    try:
        create_user(db, first_user)
    except IntegrityError:
        pass
    db.close()


def authentication_user_and_password(db: Session, username: str, password: str) -> bool:
    result = db.query(models.UserModel).filter(models.UserModel.username == username).first()
    if result is None:
        return False
    return result.password == security.calculate_hash(password)


def get_one_game(db: Session, game_id: int) -> Optional[models.GameModel]:
    result = db.query(models.GameModel).filter(models.GameModel.id == game_id).first()
    return result


def get_games(db: Session) -> List[models.GameModel]:
    return db.query(models.GameModel).order_by(models.GameModel.price.desc()).all()


def delete_one_game(db, game_id: int) -> int:
    count = db.query(models.GameModel).filter(models.GameModel.id == game_id).delete(synchronize_session=False)
    db.commit()
    return count
