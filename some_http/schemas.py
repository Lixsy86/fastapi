from typing import Optional

from pydantic.main import BaseModel


class Game(BaseModel):
    id: Optional[int]
    name: str
    price: int


class PostGame(BaseModel):
    name: str
    price: int


class Users(BaseModel):
    id: Optional[int]
    username: str
    password: str
