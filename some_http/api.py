from fastapi import FastAPI, Depends
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from some_http import crud, models, schemas
from some_http.crud import authentication_user_and_password
from some_http.database import SessionLocal, engine
from some_http.schemas import Game, PostGame

models.Base.metadata.create_all(bind=engine)
crud.create_first_user()

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/games")
def get_games(db: Session = Depends(get_db)):
    return crud.get_games(db)


@app.get("/games/{game_id}", response_model=Game)
def show_one_game(game_id: int, db: Session = Depends(get_db)):
    db_game = crud.get_one_game(db, game_id=game_id)
    if db_game:
        return schemas.Game(id=db_game.id, price=db_game.price, name=db_game.name)
    return JSONResponse({"details": f"Game with id {game_id} not found"}, status_code=404)


@app.post("/games", response_model=Game, status_code=201)
def add_games(game: PostGame, username: str, password: str, db: Session = Depends(get_db)):
    print(username, password)
    if authentication_user_and_password(db, username, password):
        try:
            created_game = crud.create_game(db, game)
            return Game(id=created_game.id, name=created_game.name, price=created_game.price)
        except IntegrityError:
            return_if_none = {"details": f"{game.name} is already created"}
            return JSONResponse(return_if_none, status_code=409)
    return JSONResponse({"details": "Wrong user or password"}, status_code=401)


@app.delete("/games/{game_id}", status_code=204)
def delete_one_game_by_game_name(game_id: int, username: str, password: str, db: Session = Depends(get_db)):
    if authentication_user_and_password(db, username, password):
        count = crud.delete_one_game(db, game_id=game_id)
        if count == 0:
            return_if_none = {"details": f"Game with game id {game_id} was not found"}
            return JSONResponse(return_if_none, status_code=404)
        return JSONResponse({"details": "Game was deleted successfully"}, status_code=204)
    return JSONResponse({"details": "Wrong user or password"}, status_code=401)
