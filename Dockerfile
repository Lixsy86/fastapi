FROM python:3.8

RUN pip install poetry

ENV PYTHONPATH="${PYTHONPATH}:/app"

WORKDIR /app

COPY poetry.lock .
COPY pyproject.toml .

RUN poetry export -f requirements.txt --output requirements.txt
RUN pip install -r requirements.txt
RUN pip list

COPY some_http /app/some_http
COPY gunicorn.sh /app/gunicorn.sh

CMD ["./gunicorn.sh"]