import pytest
from some_http.api import app
from fastapi.testclient import TestClient
from some_http import models
from some_http.database import SessionLocal

db = SessionLocal()


@pytest.fixture
def client() -> TestClient:
    return TestClient(app)


@pytest.fixture(autouse=True)
def clear_db():
    db.query(models.GameModel).delete(synchronize_session=False)
    db.query(models.UserModel).delete(synchronize_session=False)
    db.commit()
