test:
	./scripts/pytest.sh

build:
	./build.sh

run: build
	./dockerrun.sh

gunicorn:
	./gunicorn.sh
